import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  votes: number;
  title: string;
  link: string;

  constructor() { 
    this.votes = 0;
    this.title = "My Reddit";
    this.link = "http://roswell.pe";
  }
  
  voteUp(): boolean {
    this.votes += 1;
    return false
  }

  voteDown(): boolean {
    this.votes -= 1;
    return false
  }

  ngOnInit() {
  }

}
