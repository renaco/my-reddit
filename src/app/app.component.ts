import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  addLink(title: HTMLInputElement, link: HTMLInputElement): boolean {
    console.log(`Se agrego el link ${link.value} en el title ${title.value}`);
    return false;
  }
}
